<?php

namespace App\Controller;


use App\Entity\Booking;
use App\Entity\User;
use App\Repository\BookingRepository;
use App\Repository\UserRepository;
use App\Services\Booking\BookingWorkflowHandler;
use App\Services\Booking\cleaningBooking;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\NonUniqueResultException as NonUniqueResultExceptionAlias;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Workflow\Exception\LogicException;

class UserController extends AbstractController
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/user", name="user")
     */
    public function index()
    {
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    /**
     * @Route("/login", name="login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();



        return $this->render('user/login.html.twig',[

            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * @Route("/deconnexion" ,name="logout")
     */
    public function logout(){}


    /**
     * @Route("/userBooking/{id}/{deleted}", defaults={"deleted" : null}, name="user_booking")
     * @param User $user
     * @param $deleted
     * @param cleaningBooking $cb
     * @param ObjectManager $manager
     * @param UserRepository $userRepository
     * @return Response
     * @throws NonUniqueResultExceptionAlias
     */
    public function userBooking(
        User $user,
        $deleted,
        cleaningBooking $cb,
        ObjectManager $manager,
        UserRepository $userRepository)
    {

        # Permet d'épurer la base de données des réservation passée
        $cb->clean($manager);


        if (!empty($deleted) and $deleted == false)
        {
            $this->addFlash('success', "La réservation est enregistrée un mail de confirmation vous a été envoyé");
        }

        $liste = $user->getBooking();



        if ($this->security->isGranted('ROLE_VISITOR_ADMIN')) {

            $status = 'admin_visitor';

            $otherBooker =true;
            /* $bookers ??  */
            $firstnameAsked = 'Visitor';
            $userMail = $user->getEmail();

            $bookers = $userRepository->findBy([
                'email' => $userMail,
                'firstname' => $firstnameAsked
            ],null);


            return $this->render('user/user_booking.html.twig',[
                'status' => $status,
                'liste' => $liste,
                'user_booking' => $user,
                'other_booker' => $otherBooker,
                'visitor_admin' => $user,
                'bookers' => $bookers
            ]);

        } elseif ($this->security->isGranted('ROLE_VISITOR')) {

            $status = 'booker_visitor';
            $idVisitor = $user->getId();

            $emailVisitor = $user->getEmail();
            $firstnameVisitor = $user->getFirstname();

            $otherBooker = $userRepository->findOtherVisitor($idVisitor,$emailVisitor,$firstnameVisitor);

            $adminVisitor = $userRepository->findOneBy([
                'firstname' => 'Visitor_admin',
                'email' => $emailVisitor
            ],null);


            $stat = true;
        } else {
            $otherBooker = null;
            $adminVisitor = null;
            $status = null;
            $stat = false;
        }


        return $this->render(
            'user/user_booking.html.twig',
            [
                'status' => $status,
                'liste' => $liste,
                'user_booking' => $user,
                'other_booker' => $otherBooker,
                'visitor_admin' => $adminVisitor

            ]
        );
    }

    /**
     * @Route("/viewHistory/{id}/{bookingid}",defaults={"bookingid": null },name="user_bookings_history")
     * @param User $user
     * @param $bookingid
     * @param BookingRepository $repoBooking
     * @return Response
     */
    public function bookingsInHistory(User $user,$bookingid,BookingRepository $repoBooking)
    {
        if (null != $bookingid) {

            $bookingForRate = $repoBooking->find($bookingid);

            return $this->render('user/view_bookings_history.html.twig',[
                'id' => $user->getId(),
                'bid' => $bookingForRate
            ]);
        }

        return $this->render('user/view_bookings_history.html.twig',[
            'user_booking' =>$user
        ]);
    }

    /**
     * Get the vote from history
     * @Route("/checkRate",name="booking_rate")
     * @param Request $request
     * @param BookingRepository $bookingRepository
     * @param ObjectManager $manager
     * @return RedirectResponse
     */
    public function checkRateForBooking(Request $request,BookingRepository $bookingRepository,ObjectManager $manager)
    {

        $idbooking = $request->request->get('idb');
        $historyBooking = $bookingRepository->find($idbooking);
        $userid = $this->getUser()->getId();
        $rating = $request->request->get('rating');

        #check if vote has ever made
        if (null == $historyBooking->getRate()) {

            $historyBooking->setRate($rating);
            $manager->flush();
            $this->addFlash('success', "Merci, votre vote est enregistré");
        } else {
            $this->addFlash('warning', "Vous avez déjà voté !");
        }

        return $this->redirectToRoute('user_bookings_history',['id' => $userid]);
    }

    /**
     * @Route("/abortWaitingList/{status}/{id}", name="wl_aborted")
     * @param string $status
     * @param Booking $booking
     * @param BookingWorkflowHandler $bwh
     * @param ObjectManager $manager
     * @return RedirectResponse
     */
    public function abortWaitingList($status,Booking $booking, BookingWorkflowHandler $bwh,ObjectManager $manager)
    {
        try {

           $bwh->handle($booking,$status);
           $booking->setUserwaitinglist(null);
           $manager->flush($booking);

           $this->addFlash('success', "La liste d'attente est bien abandonnée.");

        } catch (LogicException $e) {
            $this->addFlash('error', "Impossible d'annuler la liste d'attente");
        }

        $userId = $this->getUser()->getId();
        $deleted = false;

        return $this->redirectToRoute('user_booking',[
            'id'=>$userId,
            'deleted' =>$deleted
        ]);
    }

    /**
     * @param BookingWorkflowHandler $bwh
     * @param ObjectManager $manager
     * @param Booking $booking
     * @param $admin
     * @param $status
     * @return RedirectResponse
     * @Route("/deleteUserBooking/{id}/{status}/{admin}",defaults={"admin" : null },requirements={"admin" : "\d+" }, name="delete_user_booking")
     */
    public function deleteUserBooking(BookingWorkflowHandler $bwh,ObjectManager $manager, Booking $booking,$admin,$status)
    {
        $user = $this->getUser();

        try {
            $bwh->handle($booking,$status);

                if (null == $booking->getId()) {
                    $this->addFlash('success',"Votre réservation à été annulée, \n le créneau est désormais disponible. ");

                }else {

                    $content_status = $booking->getStatus();
                    if ($content_status == 'room_has_been_confirmed') {
                        $this->addFlash('success', "Votre réservation à été annulée, \n une personne a pu en bénéficier. ");
                        $manager->flush($booking);
                    }
                }


        } catch (LogicException $e) {
            $this->addFlash('error',"La réservation ne peut être annulée");
        }
        $deleted = true;

        // If delete user booking come from admin
        if (!null == $admin) {
            $lastNameUser = $booking->getUser()->getLastname();
            $firstNameUser = $booking->getUser()->getFirstname();
            $this->addFlash('success',"La réservation de $firstNameUser $lastNameUser à bien été supprimée");
            return $this->redirectToRoute('admin_edit_user',[

            ]);
        }

        return $this->redirectToRoute('user_booking', [
            'id'=> $user->getId(),
            'deleted' =>$deleted

        ]);

    }

}