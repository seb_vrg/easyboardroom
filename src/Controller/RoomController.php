<?php

namespace App\Controller;

use App\Entity\Room;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
;use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


class RoomController extends AbstractController
{
    /**
     * @Route("/room", name="room_index", defaults={"id": null}, requirements={"id": "\d+"})
     * @return Response
     */
    public function index()
    {
        return $this->render('room/index.html.twig',
            [
            'controller_name' => 'RoomController',
            ]);
    }


    /**
     * @Route("/room/view/{id}/{startdate}/{duration}",defaults={"duration":null}, name="view_room")
     * @param Request $request
     * @param Room $room
     * @param $startdate
     * @param $duration
     * @return Response
     */
    public function viewRoom(Request $request,Room $room,$startdate,$duration)
    {



        $datas = $request->request->all();
        if ($datas) {

        }


        return $this->render('room/view_room.html.twig',[
            'room' => $room,
            'date_start' =>$startdate,
            'duration' =>$duration
        ]);

    }

    /**
     * @Route("/room/viewFromPanel/{id}/{idvisitor}",name="view_room_from_panel_visitor")
     * @param Room $room
     * @param $idvisitor
     * @return Response
     */
    public function viewRoomFromPanelVisitorAdmin(Room $room,$idvisitor)
    {


        return $this->render('room/view_room_from_panel.html.twig',[
            'room' =>$room,
            'id_admin' => $idvisitor
        ]);
    }
}
