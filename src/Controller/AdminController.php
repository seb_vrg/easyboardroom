<?php

namespace App\Controller;

use App\Entity\Image;
use App\Entity\Room;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response as Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Services\Room\ImageHandler;
use App\Form\RoomType;
use App\Repository\RoomRepository;



/**
 * @IsGranted("ROLE_ADMIN")
 * Class AdminController
 * @package App\Controller
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
     * @Route("/admin/register", name="admin_register")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     *
     *
     * @return Response
     */
    public function register(Request $request,
                             EntityManagerInterface $em,
                             UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $password = $userPasswordEncoder->encodePassword(
                    $user,
                    $user->getPlainPassword()
                );
                $user->setPassword($password);
                $em->persist($user);
                $em->flush();

                $this->addFlash('success', 'Un nouvel accès a été créé');
                return $this->redirectToRoute('admin_edit_user');
            } else {
                $this->addFlash('error', 'Le formulaire contient des erreurs !');
            }
        }

        return $this->render('admin/register.html.twig', [
            'id' => 'register',
            'form_register' => $form->createView()
        ]);

    }


    /**
     * @IsGranted("ROLE_ADMIN"))
     * @Route("/admin/edit/user/{id}",defaults={"id" : null },requirements={"id" : "\d+"},name="admin_edit_user")
     * @param Request $request
     * @param UserRepository $repository
     * @param EntityManagerInterface $manager
     * @param $id
     * @return Response
     */
    public function editUser(Request $request,
                             UserRepository $repository,
                             EntityManagerInterface $manager,
                             $id)

    {

        $listUsers = $repository->findBy([],[
            'id' => 'ASC'
        ]);


        if ($id) {
            $user = $manager->find(User::class,$id);

            $form = $this->createForm(UserType::class,$user);
            $form->handleRequest($request);

            if ($form->isSubmitted()) {

                if($form->isValid()) {

                    $lastNameUser = $user->getLastname();
                    $firstNameUser = $user->getFirstname();

                    $manager->persist($user);
                    $manager->flush();

                    $this->addFlash('success',"<p class='text-center'>L'utilisateur <strong>$firstNameUser $lastNameUser</strong> est modifié.</p>");

                    return $this->render('admin/list_users.html.twig',[
                        'list' => $listUsers
                    ]);

                }
            }

            return $this->render('admin/edit_user.html.twig',[
                'form_edit_user' => $form->createView()
            ]);
        }

        return $this->render('admin/list_users.html.twig',[
            'list' => $listUsers
        ]);


    }


    /**
     * @Route("/admin/delete/user/{id}", name="admin_delete_user")
     * @param User $user
     * @param EntityManagerInterface $manager
     *
     * @return Response
     */
    public function deleteUser(User $user,EntityManagerInterface $manager)
    {

        $booking = $user->getBooking();
        $nbBooking = count($booking);

        $lastNameUser = $user->getLastname();
        $firstNameUser = $user->getFirstname();

        if ($nbBooking == 0) {

            $manager->remove($user);
            $manager->flush();
            $this->addFlash('success',
                "<p class='text-center'>L'utilisateur <strong>$firstNameUser $lastNameUser</strong> a bien été supprimé.</p>");


        } else {
            $mess = "il y a des réservations";
            /* TO DO: check with booking(s)  */
        }

        return $this->render('layout2.html.twig', ['_fragment' => 'editusers']);
    }


    /**
     * @Route("/admin/list_rooms/{action}",defaults={"action": null},requirements={"action" : "\d+"},name="list_rooms")
     * @param RoomRepository $roomRepository
     * @return Response
     */
        public function listRooms(RoomRepository $roomRepository,$action)
        {
            if (!null == $action ) {
                if ($action == 1) {
                    $this->addFlash(
                        'success',
                        'Félicitations ! Une nouvelle salle a été ajoutée !!'
                    );
                } elseif ($action == 2) {
                    $this->addFlash(
                        'success',
                        'modification enregistée'
                    );
                } elseif ($action == 3) {
                    $this->addFlash('success', "La salle est supprimée");
                }
            }

            $listRooms = $roomRepository->findBy([], ['name' => 'ASC'] );

            return $this->render('admin/list_rooms.html.twig',[
                'list' => $listRooms
            ]);


        }



        /**
         * @Route("/admin/add_room", name="add_room")
         * @param Request $request
         * @param EntityManagerInterface $manager
         * @param ImageHandler $handler
         * @return Response
         */
        public function createRoom(Request $request,
                                   EntityManagerInterface $manager,
                                   ImageHandler $handler)
        {
            // $path = $this->getParameter('kernel.project_dir') . '/public/images/room';
            $form = $this->createForm(RoomType::class);  // null, ['path' => $path]);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $room = $form->getData();
                $user = $this->getUser();

                $room->setUser($user);

                //dd($room); Prend en entré l'image uploadé UploadedImage et sa retourne le nom du fichier uploadé par exmepel toto.jpg
                //$room->setImage( $imageHandler->upload( $room->getImage() ) );



                /** @var Image $image */
                $image = $room->getImage();


                $room->setImage( $handler->handle($image) );

                $manager->persist($room);
                $manager->flush();

                $this->addFlash(
                    'notice',
                    'Félicitations, Une nouvelle salle a été ajoutée !'
                );


                return $this->redirectToRoute('list_rooms',['action' => 1]);
            }
            return $this->render('admin/add_room.html.twig', [
                'form' => $form->createView(),
            ]);
        }



    /**
     * @Route("/admin/edit_room/{id}", name="edit_room", defaults={"id": null}, requirements={"id": "\d+"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param Room $room
     * @return RedirectResponse|Response
     */
        public function editRoom(
            Request $request,
            EntityManagerInterface $em,
            Room $room)
        {
            $path = $this->getParameter('kernel.project_dir') . '/public/images/room';
            $form = $this->createForm(RoomType::class, $room, ['path' => $path]);


            $form->handleRequest($request);

           if ($form->isSubmitted() && $form->isValid()) {
                $path = $this->getParameter('kernel.project_dir') . '/public/images/room';

                    $em->flush();
               $this->addFlash(
                   'notice',
                   'Félicitations ! la salle est bien modifiée !!'
               );


                    return $this->redirectToRoute('list_rooms',['action' => 2]);
                }

            return $this->render(
                'admin/edit_room.html.twig',
                [
                    'room' => $room,
                    'form' => $form->createView()
                ]);
        }



        /**
         * @Route("/admin/delete_room/{id}", name="delete_room")
         * @param EntityManagerInterface $em
         * @param Room $room
         * @return RedirectResponse
         */
        public function deleteRoom(
            EntityManagerInterface $em,
            Room $room)
        {
            $em->remove($room);
            $em->flush();

            return $this->redirectToRoute('list_rooms',['action' => 3]);
        }



    }





