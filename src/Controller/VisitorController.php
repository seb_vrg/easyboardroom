<?php


namespace App\Controller;


use App\Entity\User;
use App\Form\AvailabilityType;
use App\Repository\BookingRepository;
use App\Repository\RoomRepository;
use App\Repository\UserRepository;
use App\Services\Booking\cleaningBooking;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\String_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authorization\Voter\RoleVoter;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class VisitorController extends AbstractController
{
    private $security;
    private $lastnameBooker1 = "Booker1";
    private $lastnameBooker2 = "Booker2";


    /**
     * VisitorController constructor.
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;

    }

    /**
     * @return string
     */
    public function getLastnameBooker1(): string
    {
        return $this->lastnameBooker1;
    }

    /**
     * @return string
     */
    public function getLastnameBooker2(): string
    {
        return $this->lastnameBooker2;
    }

    /**
     * @Route("/homeVisitor", name="home_visitor")
     * @param UserRepository $userRepository
     * @param BookingRepository $bookingRepository
     * @return Response
     */
    public function index(
        UserRepository $userRepository,
        BookingRepository $bookingRepository
    )
    {

        $userId = $this->getUser()->getId();
        $user = $userRepository->find($userId);
        $mailUser = $user->getEmail();

        if ($this->isGranted('ROLE_VISITOR_ADMIN')) {

            $bookingsAdminVisitor = $bookingRepository->findBookingsForUser($userId);
            $nbBookingsAdminVisitor = count($bookingsAdminVisitor);


            $prefix = explode("@",$mailUser);
            $prefixLastnameBookers = $prefix[0];


            $lastnameBooker1 = $this->getLastnameBooker1()."_".$prefixLastnameBookers;
            $lastnameBooker2 = $this->getLastnameBooker2()."_".$prefixLastnameBookers;



            $booker1 = $userRepository->findOneBy([
                'lastname' => $lastnameBooker1,
                'email' => $mailUser
            ]);
            $booker1Id = $booker1->getId();
            $booker2 = $userRepository->findOneBy([
                'lastname' => $lastnameBooker2,
                'email' => $mailUser
            ]);
            $booker2Id = $booker2->getId();

            $bookingsBooker1 = $bookingRepository->findBookingsForUser($booker1Id);
            $nbBookingsBooker1 = count($bookingsBooker1);
            $bookingsBooker2 = $bookingRepository->findBookingsForUser($booker2Id);
            $nbBookingsBooker2 = count($bookingsBooker2);

            $totalBookings = $nbBookingsAdminVisitor + $nbBookingsBooker1 + $nbBookingsBooker2;

            if ($totalBookings > 0) {
                return $this->redirectToRoute('user_booking',[
                    'id' => $userId
                ]);
            }

        } else {
            // IF BOOKER
//            dump($user);

        }

        return $this->render('visitor/index.html.twig',[
            'visitor' => $user
        ]);
    }

    /**
     * @Route("/homeVisitor/Step2",name="register_visitor")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param UserRepository $repository
     * @return Response
     */
    public function editVisitorStepTwo(Request $request,
                                       EntityManagerInterface $manager,
                                       UserPasswordEncoderInterface $passwordEncoder,
                                       UserRepository $repository)
    {

        $mailVisitorAdmin = $request->request->get('email_visitor_admin');



        if (!empty($mailVisitorAdmin)) {

            $verifVisitor = $repository->checkUserByMail($mailVisitorAdmin);


            if (empty($verifVisitor)) {
                $visitorAdmin = new User();
                $booker1 = new User();
                $booker2 = new User();

                $passwordForAll = "easyone";
                $passwordAdminVisitor = $passwordEncoder->encodePassword($visitorAdmin,$passwordForAll);

                $bookersfirstname = "Visitor";


                $extractedPseudoBookers= explode("@",$mailVisitorAdmin);
                $pseudoBookersExtension= $extractedPseudoBookers[0];

                $lastNameBooker1 = "Booker1_".$pseudoBookersExtension;

                $lastNameBooker2 = "Booker2_".$pseudoBookersExtension;



                $bookersRole = "ROLE_VISITOR";


                $booker1->setFirstname($bookersfirstname);
                $booker1->setLastname($lastNameBooker1);
                $booker1->setEmail($mailVisitorAdmin);
                $booker1->setRoles([$bookersRole]);
                $booker1->setPassword($passwordAdminVisitor);

                $booker2->setFirstname($bookersfirstname);
                $booker2->setLastname($lastNameBooker2);
                $booker2->setEmail($mailVisitorAdmin);
                $booker2->setRoles([$bookersRole]);
                $booker2->setPassword($passwordAdminVisitor);


                $firstnameAdminVisitor = "Visitor_admin";
                $lastnameAdminVisitor = "EasyBoardRoom";
                $roleAdminVisitor = "ROLE_VISITOR_ADMIN";

                $visitorAdmin->setFirstname($firstnameAdminVisitor);
                $visitorAdmin->setLastname($lastnameAdminVisitor);
                $visitorAdmin->setEmail($mailVisitorAdmin);
                $visitorAdmin->setRoles(array($roleAdminVisitor));
                $visitorAdmin->setPassword($passwordAdminVisitor);

                $manager->persist($visitorAdmin);
                $manager->persist($booker1);
                $manager->persist($booker2);
                $manager->flush();

                if (!null == $visitorAdmin->getId()) {
                    $message = "utilisateurs enregistrés";
                } else {
                    $message = "un problème est survenu";
                }

                return $this->render('visitor/infos_for_booking.html.twig',[
                    'admin' => $visitorAdmin,
                    'booker1' => $booker1,
                    'booker2' => $booker2,

                ]);
            } else {

                $visitors = $repository->checkUserByMail($mailVisitorAdmin);

                 return $this->render('visitor/infos_for_booking.html.twig',[
                     'visitors' => $visitors,
                     'admin' =>$visitors[0],
                     'booker1' => $visitors[1],
                     'booker2' => $visitors[2]
                 ]);

            }


        } else {

            $this->addFlash('danger','Attention un des champs est vide !');
            return $this->redirectToRoute('home_visitor');

        }

    }

    /**
     * @Route("homeVisitor/Step3/{idVisitorAdmin}/{idVisitorBooker}",name="change_visitor_status")
     * @param int $idVisitorAdmin
     * @param int $idVisitorBooker
     * @param UserRepository $userRepository
     * @return Response
     * @throws \Exception
     */
    public function changeStatusVisitorStepThree(
        int $idVisitorAdmin,
        int $idVisitorBooker,
        UserRepository $userRepository)
    {
        $visitorAdmin = $userRepository->find($idVisitorAdmin);
        $visitorBooker = $userRepository->find($idVisitorBooker);

        // we clear the actual session
        $this->get('session')->clear();

        //we log the new Visitor Admin instead off
        $relogToken = new UsernamePasswordToken(
            $visitorBooker,
            null,
            'main',
            $visitorBooker->getRoles());

        $this->get('security.token_storage')->setToken($relogToken);
        $this->get('session')->set('_security_main',serialize($relogToken));

        $calendar = $this->createForm(AvailabilityType::class,
            ['dateStart' => new \DateTime()
            ],[
                'action' => $this->generateUrl('booking_availability'),
                'method' => 'GET'
            ]
            );


        return $this->render('visitor/create_my_booking.html.twig',[
          'visitor_admin' => $visitorAdmin,
            'visitor_booker_chosen' => $visitorBooker,
            'form' => $calendar->createView()

        ]);
    }


    /**
     * @Route("homeVisitor/changeStatus/{id}/{idvisitorasked}/",name="change_status")
     * @isGranted("ROLE_VISITOR")
     * @param User $currentVisitor
     * @param int $idvisitorasked
     * @param UserRepository $userRepository
     * @return Response
     */
    public function changeStatutBetweenVisitors(
        User $currentVisitor,
        int $idvisitorasked,
        UserRepository $userRepository)

    {

        $visitorAsked = $userRepository->find($idvisitorasked);


            $this->get('session')->clear();
            $switchUser = new UsernamePasswordToken(
                $visitorAsked,
                null,
                'main',
                $visitorAsked->getRoles()
            );
            $this->get('security.token_storage')->setToken($switchUser);
            $this->get('session')->set('_security_main',serialize($switchUser));

            return $this->redirectToRoute('user_booking',[
                'id' => $visitorAsked->getId()
            ]);

    }

    /**
     * @Route("homeVisitor/panelAdmin/{id}",name="panel_admin")
     * @isGranted("ROLE_VISITOR_ADMIN")
     * @param User $userAdmin
     * @param BookingRepository $bookingRepository
     * @param UserRepository $userRepository
     * @param RoomRepository $roomRepository
     * @return Response
     */
    public function displayPanelAdminVisitor(
        User $userAdmin,
        BookingRepository $bookingRepository,
        UserRepository $userRepository,
        RoomRepository $roomRepository)

    {
        $statusInProgress = "room_booked";
        $statusBookingsDone = "room_has_been_booked";
        $adminId = $userAdmin->getId();
        $mailVisitors =$userAdmin->getEmail();

        $prefix = explode("@",$mailVisitors);
        $lastNamePrefix= $prefix[0];
        $lastNameBooker1= 'Booker1_'.$lastNamePrefix;
        $lastNameBooker2= 'Booker2_'.$lastNamePrefix;

        $booker1 = $userRepository->findOneBy([
          'lastname' => $lastNameBooker1,
          'email' => $mailVisitors
        ]);

        $booker1Id = $booker1->getId();
        $booker2 = $userRepository->findOneBy([
            'lastname' => $lastNameBooker2,
            'email' => $mailVisitors
        ]);

        $booker2Id = $booker2->getId();

        $bookingsAdmin = $bookingRepository->findBookingsInProgessVisitor($adminId,$statusInProgress);
        $bookingsBooker1 = $bookingRepository->findBookingsInProgessVisitor($booker1Id,$statusInProgress);
        $bookingsBooker2 = $bookingRepository->findBookingsInProgessVisitor($booker2Id,$statusInProgress);

        $durationCumulAdmin = $bookingRepository->findBookingsInProgessVisitor($adminId,$statusBookingsDone);
        $durationCumulBooker1 = $bookingRepository->findBookingsInProgessVisitor($booker1Id,$statusBookingsDone);
        $durationCumulBooker2 = $bookingRepository->findBookingsInProgessVisitor($booker2Id,$statusBookingsDone);

        #Total Réservations effectuées
        $bookingTotalDone = $bookingRepository->findBy([
            'status' => 'room_has_been_booked'
        ]);
        #nb Total de Réservations effectuées
        $nbBookingTotalDone = count($bookingTotalDone);



        $listRooms = $roomRepository->findAll();
        $nbRooms= count($listRooms);
        $tabRoomsStat = [];


        for ( $x = 0;$x < $nbRooms;$x ++){


            #Reservation
            $resPerRoom =  count($bookingRepository->findTotalBookingsDonePerRoom($listRooms[$x]->getId()));
            $tauxResa = ($resPerRoom * 100) / $nbBookingTotalDone;

            #Quality
            $totalRatePerRoom = $bookingRepository->calculRatePerRoomBooked($listRooms[$x]->getId());
            $listRatePerRoom= $bookingRepository->getNbRateperRoom($listRooms[$x]->getId());

            $nbRatePerRoom = count($listRatePerRoom);
            $maxRatePerRoom = $nbRatePerRoom * 5;


            if ($maxRatePerRoom > 0 )
            {
                $qualityTauxPerRoom = ($totalRatePerRoom * 100 ) / $maxRatePerRoom;
            } else {
                $qualityTauxPerRoom = "néant";
            }


            array_push($tabRoomsStat,[
                'id' => $listRooms[$x]->getId(),
                'name' => $listRooms[$x]->getName(),
                'image' => $listRooms[$x]->getImage()->getName(),
                'totalBooking' => $resPerRoom,
                'taux' => $tauxResa,
               'quality' => $qualityTauxPerRoom,
                'maxrate' => $nbRatePerRoom * 5

            ]);

        }


        return $this->render('visitor/panel_admin.html.twig',[
            'visitor_admin' => $userAdmin,
            'lastname_booker1'=> $lastNameBooker1,
            'lastname_booker2' => $lastNameBooker2,
            'bookings_admin' => $bookingsAdmin,
            'bookings_booker1' => $bookingsBooker1,
            'bookings_booker2' => $bookingsBooker2,
            'duration_admin' => $durationCumulAdmin,
            'duration_booker1' => $durationCumulBooker1,
            'duration_booker2' => $durationCumulBooker2,
            'list_rooms' => $listRooms,
            'tab_room' => $tabRoomsStat,
            'nb_total_bookings_done' => $nbBookingTotalDone
        ]);
    }
}