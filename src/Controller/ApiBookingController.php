<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\User;
use App\Repository\BookingRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ApiBookingController extends AbstractController
{
    /**
     * @Route("/api/booking", name="api_booking", methods={"GET"})
     * @param BookingRepository $bookingRepository
     * @param SerializerInterface $serializer
     * @return JsonResponse
     * @throws \Exception 
     */
    public function todayPlanning(BookingRepository $bookingRepository, SerializerInterface $serializer)
    {
        $dateOfDay = new \DateTime();
        $dateOfDay->format('d-m-Y');
        $dateOfDay->setTime(00, 00, 00);


        $todaybooking = $bookingRepository->findBy([
            'dateStart' => $dateOfDay
        ],[]);
        $resultat = $serializer->serialize(
            $todaybooking,
            'json',
            [
                'groups'=>['todayBooking']
            ]
        );

        return new JsonResponse($resultat, 200,[], true);
    }

    /**
     * @Route("/api/user/{id}", name="api_user_booking", methods={"GET"})
     * @param User $user
     * @param SerializerInterface $serializer
     * @return JsonResponse 
     */
    public function userBooking(User $user, SerializerInterface $serializer)
    {

        $resultat = $serializer->serialize(
            $user,
            'json',
            [
                'groups'=>['userBooking']
            ]
        );

        return new JsonResponse($resultat, Response::HTTP_OK,[], true);
    }


    /**
     * @Route("/api/booking/{id}", name="api_delete_booking", methods={"DELETE"})
     * @param Booking $booking
     * @param ObjectManager $manager
     * @return JsonResponse 
     */
    public function deleteBooking(Booking $booking, ObjectManager $manager)
    {

        $manager -> remove($booking);
        $manager -> flush();

        return new JsonResponse("La réservation a bien été supprimée", Response::HTTP_OK,[]);
    }

}
