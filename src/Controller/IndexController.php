<?php

namespace App\Controller;

use App\Services\Index\MailMessageFromContact;
use Swift_TransportException as Swift_TransportExceptionAlias;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;



class IndexController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $isVisitor = $this->isGranted('ROLE_VISITOR');
        $isAdmin = $this->isGranted('ROLE_ADMIN');


        if ($isAdmin == true) {
            return $this->render('index/index.html.twig', [
                'controller_name' => 'IndexController',
            ]);
        }
        if ($isVisitor == true)
        {
           return $this->redirectToRoute('home_visitor',[]);
        } else {
            return $this->render('index/index.html.twig', [
                'controller_name' => 'IndexController',
            ]);
        }



    }

    /**
     * @Route("/contactUs",name="contact_us")
     * @param Request $request
     * @param MailMessageFromContact $mailMessageFromContact
     * @return Response
     */
    public function contactUs(
        Request $request,
        MailMessageFromContact $mailMessageFromContact)
    {

        $formContact = $this->createFormBuilder()
            ->add('mail',EmailType::class,[
                'label' => 'Email',
                'required' => true
            ])
            ->add('message',TextareaType::class,[
                'required' => true
            ])
            ->getForm();

        if ($request->isMethod('POST'))
        {
            $datas = $request->request->get('form');
            $mailToContact = $datas['mail'];
            $messageToContact = $datas['message'];

            if (!empty($mailToContact && $messageToContact)) {

                try{

                    $mailMessageFromContact->sendMessage($mailToContact,$messageToContact);
                    $this->addFlash('success',"Votre message a bien été envoyé");
                    return $this->redirectToRoute('home');
                }catch (\Exception $e)  {
                    $response = $e->getMessage() ;
                    $this->addFlash('warning',"Votre message n'a pu être envoyé<br>Message : $response");
                    return $this->redirectToRoute('home');

                }


            } else {
                $this->addFlash('warning', "Un des champs est vide !");
            }
        }


       return $this->render('index/contact_us.html.twig',[
           'form' => $formContact->createView()
       ]);
    }
}
