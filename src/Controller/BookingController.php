<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\Room;
use App\Form\AvailabilityType;
use App\Repository\BookingRepository;
use App\Repository\RoomRepository;
use App\Repository\TimetableRepository;
use App\Services\Booking\BookingWorkflowHandler;
use App\Services\Booking\EventListener\ConfirmationBooking;
use App\Services\Booking\MailConfirmationForBooking;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Workflow\Exception\LogicException;


/**
 * Class BookingController
 * @Route("/booking", name="booking")
 * @package App\Controller
 */
class BookingController extends AbstractController
{
    /**
     * @Route("/create", name="_create", methods={"GET"})
     */
    public function create()
    {
        $form = $this->createForm(AvailabilityType::class,
            [
                'dateStart' => new \DateTime()
            ],
            [
                'action' => $this->generateUrl('booking_availability'),
                'method' => 'GET',
            ]);

        return $this->render('booking/manage_booking.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/availability",name="_availability", methods={"GET"})
     * @param Request $request
     * @param TimetableRepository $timetableRepository
     * @param RoomRepository $roomRepository
     * @return Response
     * @throws \Exception
     */
    public function createManageBooking(
        Request $request,
        TimetableRepository $timetableRepository,
        RoomRepository $roomRepository)
    {
        #to display only timeslot in terms of actual time
        $timeSlotActual = new \DateTime();

            $fromViewRoom = $request->query->get('fromViewRoom');

            if ($fromViewRoom == true) {
                $date_start = $request->query->get('dateStart');
                $duration =$request->query->get('duration');
            } else {
                $availability = $request->query->get('availability');

                $date_start = $availability['dateStart'];

                $duration = $availability['duration'];
            }

        $start_date_converted = \DateTime::createFromFormat('d-m-Y', $date_start);
        $start_date_converted->setTime(00, 00, 00);

        return $this->render('booking/display_planing_for_booking.html.twig', [
            'planing_date' => $start_date_converted,
            'timestamp' => $start_date_converted->getTimestamp(),
            'duration' => $duration,
            'rooms' => $roomRepository->findAll(),
            'timetable' => $timetableRepository->findBy([], ['hourStart' => 'ASC']),
            'timeslot_actual'=> $timeSlotActual

        ]);

    }

    /**
     * @Route("/display/{start}",defaults={"start": null },name="_display")
     * @param RoomRepository $roomRepository
     * @param TimetableRepository $timetableRepository
     * @param $start
     * @return Response
     * @throws \Exception
     */
    public function displayPlanningOfDay(
        RoomRepository $roomRepository,
        TimetableRepository $timetableRepository,$start)
    {

        $dateOfDay = new \DateTime();

        $currentDate = new \DateTime();
        $currentDate->format('d-m-Y H:i:s');


        $noConsultPlanning = new \DateTime();
        $noConsultPlanning->format('d-m-Y');
        $noConsultPlanning->setTime(19,00,00);

        if ($currentDate > $noConsultPlanning) {
            $display = false;
        } else {
            $display =true;
        }

        #to display only timeslot in terms of actual time
        $timeSlotActual = new \DateTime();


        if (is_null($start)) {

            $dateOfDay->format('d-m-Y');
            $dateOfDay->setTime(00, 00, 00);


        } else {
            $convertDate =  date('d-m-Y',$start);
            $dateOfDay = \DateTime::createFromFormat('d-m-Y', $convertDate);

        }

        $dateOfDay->setTime(00, 00, 00);

         $this->addFlash('info',"Voici le planning du jour.Merci de vous connecter afin d'effectuer une réservation.");

        return $this->render('booking/planning_of_day.html.twig',[
            'display' => $display,
            'planing_date' => $dateOfDay,
            'timeslot_actual'=> $timeSlotActual,
            'list_rooms' => $roomRepository->findAll(),
            'timetable' => $timetableRepository->findBy([], ['hourStart' => 'ASC'])
        ]);
    }


    /**
     *
     * @Route("/book/{id}-{dateStart}-{hourStart}-{duration}.html", name="_book", methods={"GET"})
     * @param Room $room
     * @param $dateStart
     * @param $hourStart
     * @param $duration
     * @param BookingRepository $bookingRepository
     * @param BookingWorkflowHandler $bwh
     * @param MailConfirmationForBooking $mcfb
     * @return Response
     * @throws \Exception
     */
    public function bookRoom(
        Room $room,
        $dateStart,
        $hourStart,
        $duration,
        BookingRepository $bookingRepository,
        BookingWorkflowHandler $bwh,
        MailConfirmationForBooking $mcfb
       )
    {

        $start = new \DateTime();
        $start->setTimestamp($dateStart);
        $start->setTime(00, 00, 00);

        $roomId =  $room->getId();

        $hour = \DateTime::createFromFormat('H:i', $hourStart);

        # check if booking is ever made (case refresh browser)
        $checkHasBooked = $bookingRepository->findBookingsByRoomAndDate($roomId,$start,$hour);


        if (!empty($checkHasBooked) )
        {
            $this->addFlash('danger',"Votre réservation est déjà effectuée");

            return $this->redirectToRoute('booking_display');

        }

        $user = $this->getUser();

        $hour->sub(new \DateInterval('PT1H'));

        for ($i = 1; $i <= $duration; $i++) {

            $booking = new Booking();

            $booking->setUser($user)
                ->setRoom($room)
                ->setDateStart($start)
                ->setHourStart($hour)
                ->setDuration(1)
            ;
            $booking->setHourStart($hour->add(new \DateInterval('PT1H')));
            $room->addBooking($booking);
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($booking);

            $manager->flush();

            $status = $booking->getStatus();

            # On récupère le status de Booking juste après l'insertion en base (donc par défaut "room_available")
            # Et si l'enregitrement à bien eu lieu, on change le status (avec le service $bwh) tout de suite à "room_booked"

            if (!null == $booking->getId()) {
                try{
                    $bwh->handle($booking, $status);
                    $mcfb->sendConfirmation($booking);
                  //  return $this->render('booking/confirmation_booking__by_mail.html.twig');

                } catch (LogicException $e) {
                    $this->addFlash('error', "Changement de status impossible.");
                }
            }
        }
        $userId = $this->getUser()->getId();
        $deleted = false;
        $this->addFlash('success',"Votre réservation est bien enregistée.");

        return $this->redirectToRoute('user_booking',[
            'id'=>$userId,
            'deleted' =>$deleted
        ]);


    }

    /**
     * @Route("/waitingListRequested/{status}/{id}", name="_waiting_list")
     * @param $status
     * @param Booking $booking
     * @param BookingWorkflowHandler $bwh
     * @param ObjectManager $manager
     * @return RedirectResponse
     */
    public function moveToWaitingList($status, Booking $booking, BookingWorkflowHandler $bwh,ObjectManager $manager)
    {
        $currentUser = $this->getUser();

        try {
            $bwh->handle($booking,$status);

            $booking->setUserwaitinglist($currentUser);
            $manager->flush();

            $this->addFlash('success',"Votre demande de liste d'attente est enregistée.");

        } catch (LogicException $e) {
            $this->addFlash('error', "Liste d'attente impossible.");
        }

        $userId = $this->getUser()->getId();
        $deleted = false;

        return $this->redirectToRoute('user_booking',[
            'id'=>$userId,
            'deleted' =>$deleted
        ]);
    }


}