<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @property   user
 * @ORM\Entity(repositoryClass="App\Repository\RoomRepository")
 * @ApiResource()
 */
class Room
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"todayBooking","userBooking"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"todayBooking","userBooking"})
     */
    private $capacity;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"todayBooking"})
     * @Groups({"todayBooking","userBooking"})
     * @Assert\Length(
     *     min=3,
     *     max=50,
     *     minMessage="Le nom doit contenir au moins {{ limit }} caractères",
     *     maxMessage="Le nom doit contenir au plus {{ limit }} caractères"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $facilities;



    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Booking", mappedBy="room")
     * @ApiSubresource()
     */
    private $bookings;


    /**
     * @ORM\JoinColumn(name="image_id", nullable=true)
     * @ORM\OneToOne(targetEntity="Image", cascade={"persist", "remove"})
     * @Groups({"todayBooking"})
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User",inversedBy="rooms")
     */
    private $user;


    public function __construct()
    {

        $this->bookings = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFacilities(): ?string
    {
        return $this->facilities;
    }

    /**
     * @param mixed $facilities
     */
    public function setFacilities($facilities): void
    {
        $this->facilities = $facilities;
    }



    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return Collection|Booking[]
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setRoom($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->contains($booking)) {
            $this->bookings->removeElement($booking);
            // set the owning side to null (unless already changed)
            if ($booking->getRoom() === $this) {
                $booking->setRoom(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Room
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }



}