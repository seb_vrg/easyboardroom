<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ApiResource()
 * @UniqueEntity(fields={"lastname"},message="Il existe déjà un utilisateur avec ce nom !")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"todayBooking","userBooking"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"todayBooking","userBooking"})
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"todayBooking","userBooking"})
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @var array
     * @ORM\Column(name="roles", type="array", nullable=false)
     */
    private $roles = [];

    /**
     * @var string
     * @Assert\NotBlank(message="Le mot de pass est obligatoire !")
     * @Assert\Length(min="6",minMessage="Le mot de passe doit faire au moins {{ limit }} caractères")
     */
    private $plainPassword;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Booking",mappedBy="userwaitinglist")
     * @ORM\JoinColumn(name="waitinglistuser",nullable=true)
     */
    private $waitinglistuser;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Booking", mappedBy="user")
     * @Groups({"todayBooking","userBooking"})
x     */
    private $booking;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Room", mappedBy="user", cascade={"remove"})
     */
    private $rooms;

    public function __construct()
    {
        $this->booking = new ArrayCollection();
        $this->roles[]=  'ROLE_USER';
    }

    public function __toString()
    {
        return $this->firstname. ' '.$this->lastname;
    }


    /**
     * @return string
     */
    public function getPlainPassword():? string
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     * @return User
     */
    public function setPlainPassword(string $plainPassword): User
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }


    /**
     * @return array
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';
        return array_unique($roles);
    }

    /**
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles): User
    {
        $this->roles = $roles;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

   


    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {

    }

    /**
     * @return Collection|Booking[]
     */
    public function getBooking(): Collection
    {
        return $this->booking;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->booking->contains($booking)) {
            $this->booking[] = $booking;
            $booking->setUser($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->booking->contains($booking)) {
            $this->booking->removeElement($booking);
            // set the owning side to null (unless already changed)
            if ($booking->getUser() === $this) {
                $booking->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * @param mixed $rooms
     * @return User
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;
        return $this;
    }
    
     /**
     * @return mixed
     */
    public function getWaitinglistuser()
    {
        return $this->waitinglistuser;
    }

    /**
     * @param mixed $waitinglistuser
     * @return User
     */
    public function setWaitinglistuser($waitinglistuser)
    {
        $this->waitinglistuser = $waitinglistuser;
        return $this;
    }
    
    
}
