<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingRepository")
 * @ApiResource()
 * @UniqueEntity(fields={"hour_start","room_id"}, message="Il existe déjà une réservation pour cette salle à cette horaire !")
 * @ORM\HasLifecycleCallbacks()
 */
class Booking
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"todayBooking","userBooking"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"todayBooking","userBooking"})
     */
    private $bookingDate;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"todayBooking","userBooking"})
     */
    private $dateStart;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @Groups({"todayBooking","userBooking"})
     */
    private $HourStart;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Groups({"todayBooking","userBooking"})
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Room", inversedBy="bookings")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"todayBooking","userBooking"})
     */
    private $room;


    /**
     * @var integer
     * @ORM\Column(type="integer",name="duration")
     * @Groups({"todayBooking","userBooking"})
     */
    private $duration;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="waitinglistuser")
     * @ORM\JoinColumn(name="userwaitinglist",nullable=true)
     */
    private $userwaitinglist;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="booking")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"todayBooking","userBooking"})
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(type="string",name="rate", nullable=true)
     */
    private $rate;


    public function __construct()
    {
        $this->status = "room_available";
        $this->bookingDate = new \DateTime();
        $this->bookingDate->setTime(00, 00, 00);
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBookingDate(): ?\DateTime
    {
        return $this->bookingDate;
    }

    public function setBookingDate(\DateTime $bookingDate): self
    {
        $this->bookingDate = $bookingDate;

        return $this;
    }

    public function getDateStart(): ?\DateTime
    {
        return $this->dateStart;
    }

    public function setDateStart(\DateTime $dateStart): self
    {
        $this->dateStart = $dateStart;

        return $this;
    }


    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function setRoom(?Room $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getHourStart(): ?\DateTimeInterface
    {
        return $this->HourStart;
    }

    public function setHourStart(\DateTimeInterface $HourStart): self
    {
        $this->HourStart = $HourStart;

        return $this;
    }


    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration(int $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Booking
     */
    public function setStatus(string $status): Booking
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param string $rate
     * @return Booking
     */
    public function setRate(string $rate) :self
    {
        $this->rate = $rate;
        return $this;
    }
    /**
     * @return mixed
     */
    public function getUserwaitinglist()
    {
        return $this->userwaitinglist;
    }

    /**
     * @param mixed $userwaitinglist
     * @return Booking
     */
    public function setUserwaitinglist(?User $userwaitinglist)
    {
        $this->userwaitinglist = $userwaitinglist;
        return $this;
    }




}
