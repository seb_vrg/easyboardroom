<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname',TextType::class,[
                'label'=> 'Nom'
            ])
            ->add('firstname',TextType::class,[
                'label' => 'Prénom'
            ])
            ->add('email',EmailType::class,[
                'label' =>'Email'
            ])
            ->add('plainPassword',RepeatedType::class,
                [
                    'type' => PasswordType::class,
                    'first_options' => [
                        'label' => 'Mot de passe'
                    ],
                    'second_options' =>[
                        'label' => 'Répéter le mot de passe'
                    ],
                    'invalid_message' => 'les mots de passe ne sont pas identiques !'
                ])
            ->add('roles',ChoiceType::class,
                [
                    'placeholder' => 'Séléctionnez un statut',
                'required' => true,
                    'multiple' => true,
                    'choices' => [
                        'ADMINISTRATEUR' => 'ROLE_ADMIN',
                        'UTILISATEUR' => 'ROLE_USER',
                        'VISITEUR' => 'ROLE_VISITOR'
                    ]
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection'   => false
        ]);
    }
}
