<?php


namespace App\Services\Room;


use App\Repository\BookingRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class RoomTwigExtension extends AbstractExtension
{
    private $repository;

    public function __construct(BookingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('checkRoomAvailability', [$this, 'checkRoomAvailability']),
        ];
    }

    public function checkRoomAvailability(int $roomId, \DateTime $startDate, \DateTime $hourStart)
    {

        $bookings = $this->repository->findBookingsByRoomAndDate($roomId, $startDate, $hourStart);

        return $bookings ? false : true;
    }

}