<?php


namespace App\Services\Room;


use App\Entity\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class ImageHandler
{
    private $path;

    public function __construct($path)
    {
        $this->path = $path.'/public/images/room';
    }

    public function handle(UploadedFile $image)
    {
        //récupère le fichier soumis
        $name = $this->createName($image);

        //Déplace le fichier
        $image->move($this->path, $name);

        $img = new Image();
        $img->setName($name);
        $img->setPath($this->path);

        return $img;

    }

    private function createName(UploadedFile $file): string
    {
        return md5(uniqid()). $file->getClientOriginalName();
    }
}