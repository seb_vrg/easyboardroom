<?php


namespace App\Services\Index;


use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class MailMessageFromContact
{

    private $mailer;
    private $templating;

    /**
     * MailMessageFromContact constructor.
     * @param \Swift_Mailer $mailer
     * @param Environment $templating
     */
    public function __construct(\Swift_Mailer $mailer,Environment $templating)
    {
        $this->mailer= $mailer;
        $this->templating = $templating;
    }

    /**
     * @param string $mailFrom
     * @param string $messageFrom
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function sendMessage(string $mailFrom, string $messageFrom)
    {
        $message = (new \Swift_Message())
            ->setSubject('EasyBoardRoom Message')
            ->setFrom('easyboardroom@gmail.com')
            ->setTo('sebvrg@gmail.com');

        $img = $message->embed(\Swift_Image::fromPath('images/logos/logo.png'));


        $message->setBody(
            $this->templating->render('index/message_from.html.twig',[
                'logo' => $img,
                'email_from' =>$mailFrom,
                'message_from' => $messageFrom
                ])
            ,'text/html'
        );
        $type = $message->getHeaders()->get('Content-Type');
        $type->setCharset('utf8');
        $mailer = $this->mailer;
        $mailer->send($message);


        return;
    }
}