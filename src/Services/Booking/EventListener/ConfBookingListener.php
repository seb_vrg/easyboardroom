<?php


namespace App\Services\Booking\EventListener;


use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use App\Entity\Booking;


class ConfBookingListener
{

    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function postPersist(LifecycleEventArgs $args)
    {

        $entity = $args->getObject();




        if ($entity instanceof Booking) {
            $bookingId = $entity->getId();
            $userBooking = $entity->getUser();
            $firstNameUser = $userBooking->getFirstname();
            $lastNameUser = $userBooking->getLastname();
            $mailUser = $userBooking->getEmail();
            $roomName = $entity->getRoom()->getName();
            $bookingDate = $entity->getDateStart()->format('d/m/Y');
            $bookingHour = $entity->getHourStart()->format('H:i');
            /*var_dump($bookingId,$firstNameUser);
            die();*/
        }
        return;
    }
}