<?php


namespace App\Services\Booking;

use App\Entity\User;
use App\Repository\BookingRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class checkBookingInHistoryTwigExtension extends AbstractExtension
{
    private $repository;

    public function __construct(BookingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getHistoryList',[$this, 'getHistoryList'])
        ];
    }

    public function getHistoryList(User $user,string $status)
    {
        $historyList = $this->repository->getHistoryListForUser($user,$status);

        return $historyList;
    }

}