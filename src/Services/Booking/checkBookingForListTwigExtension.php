<?php


namespace App\Services\Booking;


use App\Repository\BookingRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class checkBookingForListTwigExtension extends AbstractExtension
{
    private $repository;

    public function __construct(BookingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('checkWaitingList', [$this, 'checkWaitingList'])
        ];
    }

    public function checkWaitingList(int $roomId, \DateTime $startDate, \DateTime $hourStart)
    {
        $booking = $this->repository->findBookingInWaitingList($roomId,$startDate,$hourStart);

        return $booking;
    }
}
