<?php


namespace App\Services\Booking;


use App\Repository\BookingRepository;
use Doctrine\Common\Persistence\ObjectManager;

class cleaningBooking
{

    private $repository;

    public function __construct(BookingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function clean(ObjectManager $manager):void {

        $listToHistory = $this->repository->cleanPassedBooking();

        $nbBooking = count($listToHistory);

        $historyStatus = 'room_has_been_booked';

        if ($nbBooking > 0) {
            foreach ($listToHistory as $resa) {
                $resa->setStatus($historyStatus);
                $manager->flush($resa);
            }
        }




    }
}
