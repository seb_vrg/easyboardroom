<?php


namespace App\Services\Booking;


use App\Entity\Booking;
use App\Repository\BookingRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Workflow\Registry;

class BookingWorkflowHandler
{

    private $workflows, $manager , $repository;

    /**
     * BookingWorkflowHandler constructor.
     * @param Registry $workflows
     * @param ObjectManager $manager
     * @param BookingRepository $repository
     */
    public function __construct(Registry $workflows, ObjectManager $manager,BookingRepository $repository)
    {
        $this->workflows = $workflows;
        $this->manager = $manager;
        $this->repository = $repository;
    }

    /**
     * Method to allow changing status for the booking
     * @param Booking $booking
     * @param string $status
     */
    public function handle(Booking $booking, string $status) :void
    {

        $workflow = $this->workflows->get($booking);

        $em =$this->manager;

        if ($status == 'room_available') {
            if ($workflow->can($booking, 'to_booking')) {
                $workflow->apply($booking, 'to_booking');
                $em->flush();
            }
        } elseif ($status == 'room_in_waiting_list') {
            if ($workflow->can($booking, 'to_waiting_list')) {
                $workflow->apply($booking, 'to_waiting_list');
                $em->flush();
            }
        } elseif ($status == 'room_booked') {
            if ($workflow->can($booking,'waiting_list_aborted')) {
                $workflow->apply($booking,'waiting_list_aborted');
            }
        } elseif ($status == 'booking_deleted') {

            if (!null == $booking->getUserwaitinglist()) {
                $userInList = $booking->getUserwaitinglist();

                if ($workflow->can($booking,'room_deleted_with_waiting_list')) {
                    $workflow->apply($booking, 'room_deleted_with_waiting_list');
                    $booking->setUser($userInList);
                    $booking->setUserwaitinglist(null);
                }


            } else {
                if ($booking->getStatus() == 'room_booked') {
                    if ($workflow->can($booking, 'room_deleted_no_waiting_list')) {
                        $workflow->apply($booking, 'room_deleted_no_waiting_list');
                        $em->remove($booking);
                        $em->flush();
                    }
                } elseif ($booking->getStatus() == 'room_has_been_confirmed') {
                    if ($workflow->can($booking,'room_deleted_has_been_confirmed')) {
                        $workflow->apply($booking,'room_deleted_has_been_confirmed');
                        $em->remove($booking);
                        $em->flush();
                    }
                }
            }


        }
    }

}
