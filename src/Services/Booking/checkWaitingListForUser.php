<?php


namespace App\Services\Booking;


use App\Entity\User;
use App\Repository\BookingRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class checkWaitingListForUser extends AbstractExtension
{

    private $repository;

    public function __construct(BookingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getWaitingList',[$this, 'getWaitingList'])
        ];
    }

    public function getWaitingList(User $user,string $status)
    {
        $waitingList = $this->repository->getWaitingListForUser($user,$status);
        return $waitingList;
    }
}
