<?php


namespace App\Services\Booking;


use App\Entity\Booking;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;


class MailConfirmationForBooking
{

    private $mailer;
    private $templating;

    /**
     * MailConfirmationForBooking constructor.
     * @param \Swift_Mailer $mailer
     * @param Environment $templating
     */
    public function __construct(\Swift_Mailer $mailer, Environment $templating)
    {
        $this->mailer= $mailer;
        $this->templating = $templating;

    }

    /**
     * @param Booking $booking
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function sendConfirmation(Booking $booking)
    {


        $userBooking = $booking->getUser();
        $firstNameUser = $userBooking->getFirstname();
        $lastNameUser = $userBooking->getLastname();
        $mailUser = $userBooking->getEmail();
        $roomName = $booking->getRoom()->getName();
        $dateOfBooking = $booking->getBookingDate()->format('d/m/Y');
        $bookingStart = $booking->getDateStart()->format('d/m/Y');
        $hourStart = $booking->getHourStart()->format('H:i');




        $message = (new \Swift_Message())
            ->setSubject('EasyBoardRoom Confirmation')
            ->setFrom('easyboardroom@gmail.com')
            ->setTo($mailUser);



        $img = $message->embed(\Swift_Image::fromPath('images/logos/logo.png'));
        $message->setBody(
            $this->templating->render('booking/conf_mail2.html.twig',[
                'firstName' => $firstNameUser,
                'lastName' => $lastNameUser,
                'dateOfBooking' => $dateOfBooking,
                'dateStart' => $bookingStart,
                'hourStart' => $hourStart,
                'roomName' => $roomName,
                'logo' => $img
            ])
            ,'text/html');
        $type = $message->getHeaders()->get('Content-Type');
        $type->setCharset('utf8');
        $mailer = $this->mailer;
        $mailer->send($message);


        return;
    }
}