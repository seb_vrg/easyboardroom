<?php


namespace App\Services\Booking;


use App\Entity\Booking;
use App\Entity\User;
use App\Repository\BookingRepository;

class DisplaysRoomsAvailable implements ManageBookingInterface
{
    private $bookingRepository;

    public function __construct(BookingRepository $bookingRepository)
    {
        $this->bookingRepository = $bookingRepository;
    }

    public function displayRoomsAvailable(User $user, \DateTime $dateStart, int $duration): iterable
    {

       // $tab = [];
        $planingOfDay = $this->bookingRepository-> getRoomListForDate($dateStart);


//        foreach ($planingOfDay as $item) {
//
//            $id = $item->getId();
//            $hour = $item->getHourStart();
//
//               $tab = [
//                   $id => $this->bookingRepository->hasBooking($id, $hour)
//               ];
//        }
//

        return $planingOfDay;

    }

//    public function hasBooking(\DateTime $dateStart, \DateTime $hourStart ) :Booking {
//
//
//    }

}