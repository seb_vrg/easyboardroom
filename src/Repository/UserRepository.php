<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException as NonUniqueResultExceptionAlias;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param string $mail
     * @return mixed
     */
   public function checkUserByMail(string $mail) {

        return $this->createQueryBuilder('u')
            ->where('u.email = :email')
            ->setParameter('email',$mail)
            ->getQuery()
            ->getResult()
            ;
   }

    /**
     * @param int $idCurrentVisitor
     * @param string $mailCurrentVisitor
     * @param string $firstnameCurrentVisitor
     * @return mixed
     * @throws NonUniqueResultExceptionAlias
     */
   public function findOtherVisitor(int $idCurrentVisitor,string $mailCurrentVisitor, string $firstnameCurrentVisitor)
   {
       return $this->createQueryBuilder('v')
           ->where('v.id != :current_id')
           ->setParameter('current_id',$idCurrentVisitor)
           ->andWhere('v.email = :current_mail')
           ->setParameter('current_mail',$mailCurrentVisitor)
           ->andWhere('v.firstname = :current_firstname')
           ->setParameter('current_firstname', $firstnameCurrentVisitor)
           ->getQuery()
           ->getOneOrNullResult()
           ;
   }


}
