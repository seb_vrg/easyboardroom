<?php

namespace App\Repository;

use App\Entity\Booking;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Booking::class);
    }


    /* public function hasBooked(int $idRoom,\DateTime $dateStart,\DateTime $hourStart) {

         return $this->createQueryBuilder('b')

             ->where('b.room = :idRoom')
             ->setParameter('idRoom', $idRoom)



             ;
     }*/



    public function findBookingsForUser(int $idUser)
    {
        return $this->createQueryBuilder('b')
            ->where('b.user = :idUser')
            ->setParameter('idUser',$idUser)
            ->getQuery()
            ->getResult()
            ;

    }
    public function getNbRateperRoom($idRoom)
    {
        $x = 0;

        return $this->createQueryBuilder('b')
            ->where('b.room = :idRoom' )
            ->setParameter('idRoom', $idRoom)
            ->andWhere('b.rate > :x')
            ->setParameter('x',$x)
            ->getQuery()
            ->getResult()

            ;
    }

    public function calculRatePerRoomBooked($idRoom)
    {


        return $this->createQueryBuilder('b')
            ->where('b.room = :idRoom' )
            ->setParameter('idRoom', $idRoom)
            ->select('SUM(b.rate) as totalrate')


            ->getQuery()
            ->getSingleScalarResult();

            ;
    }

    public function findTotalBookingsDonePerRoom($idRoom)
    {
        $statusDone = "room_has_been_booked";

        return $this->createQueryBuilder('b')
            ->where('b.room = :idRoom')
            ->setParameter('idRoom',$idRoom)
            ->andWhere('b.status = :done')
            ->setParameter('done',$statusDone)
            ->getQuery()
            ->getResult()
            ;
    }
    public function findBookingsInProgessVisitor(int $idVisitor,string $status)
    {

       return $this->createQueryBuilder('b')
           ->where('b.user = :idVisitor')
           ->setParameter('idVisitor',$idVisitor)
           ->andWhere('b.status = :status')
           ->setParameter('status',$status)
           ->getQuery()
           ->getResult()
       ;
    }
    public function findBookingInWaitingList(int $roomId, \DateTime $dateStart,\DateTime $hourstart)
    {
        $result = $this->findOneBy([
                'room'=> $roomId,
                'dateStart' => $dateStart,
                'HourStart' => $hourstart]
        );

        return $result;
    }
    /**
     * @param int $roomId
     * @param \DateTime $dateStart
     * @param \DateTime $hourStart
     * @return mixed
     */
    public function findBookingsByRoomAndDate(int $roomId, \DateTime $dateStart, \DateTime $hourStart) {

        return $this->createQueryBuilder('b')

            # Get bookings from roomId
            ->where('b.room = :roomId')
            ->setParameter('roomId', $roomId)

            # Filter by date
            ->andWhere('b.dateStart = :dateStart')
            ->setParameter('dateStart', $dateStart)

            # Filter by hourStart
            ->andWhere('b.HourStart = :hourStart')
            ->setParameter('hourStart', $hourStart->format('H:i'))

            ->getQuery()
            ->getResult()
            ;

    }



    /**
     * @param int $idBooking
     * @param string $status
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function checkBookingByStatus(int $idBooking, string $status) : ?Booking
    {
        return $this->createQueryBuilder('b')
            ->where('b.id = :booking_id')
            ->setParameter('booking_id', $idBooking)
            ->andWhere('b.status LIKE :status')
            ->setParameter('status', $status)
            ->getQuery()
            ->getOneOrNullResult();
    }


    /**
     * @param User $user
     * @param string $status
     * @return array|null
     */
    public function getWaitingListForUser(User $user, string $status) :?array
    {
        $result = $this->findBy([
            'userwaitinglist' => $user,
            'status' => $status
        ]);

        return $result;
    }

    /**
     * @param User $user
     * @param string $status
     * @return array|null
     */
    public function getHistoryListForUser(User $user, string $status) :?array
    {
        $result = $this->findBy([
            'user' => $user,
            'status' => $status
        ]);

        return $result;
    }

    /**
     *  Used to change booking in history
     * @return array|null
     * @throws \Exception
     */
    public function cleanPassedBooking() :?array
    {
        $todayDate = new \DateTime();
        $timeNow = new \DateTime();
        $hourNow = $timeNow->format('H:i');
        $pastStatus = 'room_has_been_booked';

        $results = $this->createQueryBuilder('b')
            ->where('b.dateStart < :today')
            ->setParameter('today', $todayDate)
            ->andWhere('b.HourStart < :hourNow')
            ->setParameter('hourNow', $hourNow)
            ->andWhere('b.status !=  :pastStatus')
            ->setParameter('pastStatus', $pastStatus)
            ->getQuery()
            ->getResult()
        ;
        return $results;
    }

    // /**
    //  * @return Booking[] Returns an array of Booking objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


}
