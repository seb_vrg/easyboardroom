# EasyBoardRoom

This is an application used to book offices or board rooms in a work space center.

1. To load files in vendor :  `composer install`
2. Update database : `bin/console doctrine:schema:update -f`
3. Insert timetable table in your db with **easyboardoom.sql**
    

