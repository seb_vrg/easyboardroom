(function ($){

    $('#oeil').click(function () {
       $('#panneau-control').toggle()
    })

    $('#view-panneau-connect').click(function () {
       $('#paneau-connection').toggle()
    })

    $('#btn-login').click(function () {
       $('#bloc-welcome').toggle()
    })

    $('#btn-infos-visitor').click(function () {
        $('#panneau-infos-visitor').toggle(1600)
    })

    $('#btn-back-users').click(function () {
        $('#bloc-activity-users').toggle(1400)
    })
    $('#btn-back-salle').click(function () {
        $('#bloc-activity-rooms').toggle(1400)
    })

    $('.btn-edit-user-visitor').click(function () {
        $('#panneau-infos-visitor').toggle(1600)
    })

    $('#plus-details').click(function () {
        $('#panneau-infos-visitor').toggle(1600)
    })

}(jQuery));